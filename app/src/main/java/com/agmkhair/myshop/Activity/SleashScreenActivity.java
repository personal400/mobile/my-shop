package com.agmkhair.myshop.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;

import android.os.Bundle;
import android.view.WindowManager;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.agmkhair.myshop.MainActivity;
import com.agmkhair.myshop.R;

public class SleashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slesh);


        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


                new Handler().postDelayed(new Runnable() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void run() {


                        Intent intent = new Intent(SleashScreenActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();

                    }

                },2000);

    }
}
